import styles from '../styles/works.module.css';
import Layout from './components/layout';
import Link from 'next/link';
import { useState } from 'react';

import { fetchWorkEntries } from './api';

export default function Works({ posts }) {
  const [filter, setFilter] = useState('all');
  const tagsArr = posts.map((p) => p.tag);
  const tags = Array.from(new Set(tagsArr));
  const filteredPosts =
    filter === 'all' ? posts : posts.filter((post) => post.tag == filter);

  return (
    <>
      <Layout>
        <div className={styles.works}>
          <div className={styles.filters}>
            <button
              className={`${styles.filter} ${
                filter === 'all' ? styles.active : ''
              }`}
              onClick={() => setFilter('all')}
            >
              All
            </button>
            {tags.map((tag) => {
              return (
                <button
                  className={`${styles.filter} ${
                    filter === tag ? styles.active : ''
                  }`}
                  key={tag}
                  onClick={() => setFilter(tag)}
                >
                  {tag}
                </button>
              );
            })}
          </div>
          <div className={styles.posts}>
            {filteredPosts.map((p) => {
              return (
                <Link key={p.slug} as={`/works/${p.slug}`} href='/works/[slug]'>
                  <a className={styles.preview}>
                    <div className={styles.overlay}></div>
                    <img
                      className={styles.image}
                      src={p.coverImage.fields.file.url}
                      alt=''
                    />
                    <div className={styles.excerpt}>
                      <h3 className={styles.title}>{p.title}</h3>
                      <p>{p.excerpt}</p>
                    </div>
                  </a>
                </Link>
              );
            })}
          </div>
        </div>
      </Layout>
    </>
  );
}

export async function getStaticProps() {
  const res = await fetchWorkEntries();
  const posts = await res.map((p) => {
    return p.fields;
  });

  return {
    props: {
      posts,
    },
  };
}
