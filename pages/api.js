const space = process.env.CONTENTFUL_SPACE_ID;
const accessToken = process.env.CONTENTFUL_ACCESS_TOKEN;

const client = require('contentful').createClient({
  space: space,
  accessToken: accessToken,
});

export async function fetchBlogEntries() {
  const entries = await client.getEntries({ content_type: 'post' });
  if (entries.items) return entries.items;
  console.log(`Error getting Entries for ${contentType.name}.`);
}

export async function fetchWorkEntries() {
  const entries = await client.getEntries({ content_type: 'work' });
  if (entries.items) return entries.items;
  console.log(`Error getting Entries for ${contentType.name}.`);
}

export async function fetchBooking() {
  const entries = await client.getEntries({ content_type: 'booking' });
  if (entries.items) return entries.items;
  console.log(`Error getting Entries for ${contentType.name}.`);
}
