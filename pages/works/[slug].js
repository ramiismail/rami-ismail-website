import styles from '../../styles/slug.module.css';
import Layout from '../components/layout';
import { documentToReactComponents } from '@contentful/rich-text-react-renderer';
import AwesomeSlider from 'react-awesome-slider';
import 'react-awesome-slider/dist/styles.css';

export default function Slug({ post }) {
  const {
    title,
    coverImage,
    content,
    credits,
    screenshots,
    link,
  } = post.fields;

  return (
    <Layout>
      <div className={styles.singlePost}>
        <img
          className={styles.headerImage}
          src={coverImage.fields.file.url}
          alt=''
        />
        <h1 className={styles.title}>{title}</h1>
        <a
          className={styles.link}
          href={link}
          target='_blank'
          rel='noopener noreferrer'
        >
          {link}
        </a>
        <div>{documentToReactComponents(content)}</div>
        {screenshots && (
          <div className={styles.screenshots}>
            <h2>Screenshots</h2>
            <AwesomeSlider cssModule={styles}>
              {screenshots.map((screenshot) => {
                return (
                  <img
                    key={screenshot.fields.title}
                    data-src={screenshot.fields.file.url}
                    alt=''
                  />
                );
              })}
            </AwesomeSlider>
          </div>
        )}

        {credits && (
          <div className={styles.credits}>
            <h2>Credits</h2>
            {credits.map((credit) => {
              return (
                <p key={credit} className={styles.credit}>
                  {credit}
                </p>
              );
            })}
          </div>
        )}
      </div>
    </Layout>
  );
}

export async function getStaticProps(context) {
  // Create an instance of the Contentful JavaScript SDK
  const client = require('contentful').createClient({
    space: process.env.CONTENTFUL_SPACE_ID,
    accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
  });

  // Fetch all results where `fields.slug` is equal to the `slug` param
  const result = await client
    .getEntries({
      content_type: 'work',
      'fields.slug': context.params.slug,
    })
    .then((response) => response.items);

  // Since `slug` was set to be a unique field, we can be confident that
  // the only result in the query is the correct post.
  const post = result.pop();

  // If nothing was found, return an empty object for props, or else there would
  // be an error when Next tries to serialize an `undefined` value to JSON.
  if (!post) {
    return { props: {} };
  }

  // Return the post as props
  return {
    props: {
      post,
    },
  };
}

export async function getStaticPaths() {
  // Create an instance of the Contentful JavaScript SDK
  const client = require('contentful').createClient({
    space: process.env.CONTENTFUL_SPACE_ID,
    accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
  });

  // Query Contentful for all blog posts in the space
  const posts = await client
    .getEntries({ content_type: 'work' })
    .then((response) => response.items);

  // Map the result of that query to a list of slugs.
  // This will give Next the list of all blog post pages that need to be
  // rendered at build time.
  const paths = posts.map(({ fields: { slug } }) => ({ params: { slug } }));

  return {
    paths,
    fallback: false,
  };
}
