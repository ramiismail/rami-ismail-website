import styles from '../styles/bookings.module.css';
import Layout from './components/layout';
import { InlineWidget } from 'react-calendly';
import { documentToReactComponents } from '@contentful/rich-text-react-renderer';
import { fetchBooking } from './api';

export default function Bookings({ booking }) {
  return (
    <Layout>
      <div className={styles.bookings}>
        <div className={styles.text}>
          {documentToReactComponents(booking[0].intro)}
        </div>
        <InlineWidget
          pageSettings={{
            backgroundColor: 'ffffff',
            hideEventTypeDetails: false,
            // hideLandingPageDetails: true,
            primaryColor: '00a2ff',
            textColor: '4d5055',
          }}
          url='https://calendly.com/tha_rami'
        />

        <div className={styles.text}>
          {documentToReactComponents(booking[0].body)}
        </div>
      </div>
    </Layout>
  );
}

export async function getStaticProps() {
  const res = await fetchBooking();
  const booking = await res.map((p) => {
    return p.fields;
  });

  return {
    props: {
      booking,
    },
  };
}
