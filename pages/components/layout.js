import styles from './layout.module.css';
import Link from 'next/link';
import Head from 'next/head';
import Menu from 'react-burger-menu/lib/menus/slide';
import { useRouter } from 'next/router';

export default function Layout({ children, about }) {
  const router = useRouter();

  const burgerStyles = {
    bmBurgerButton: {
      position: 'fixed',
      width: '26px',
      height: '20px',
      right: '18px',
      top: '20px',
    },
    bmBurgerBars: {
      background: '#373a47',
    },
    bmBurgerBarsHover: {
      background: '#a90000',
    },
    bmCrossButton: {
      width: '30px',
      height: '30px',
      right: '14px',
      top: '16px',
    },
    bmCross: {
      // background: '#bdc3c7',
    },
    bmMenuWrap: {
      position: 'fixed',
      height: '100%',
    },
    bmMenu: {
      background: '#373a47',
      padding: '2.5em 1.5em 0',
      fontSize: '1.15em',
    },
    bmMorphShape: {
      fill: '#373a47',
    },
    bmItemList: {
      color: '#b8b7ad',
      color: '#ffffff',
      padding: '0.8em',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    bmItem: {
      display: 'flex',
      justifyContent: 'center',
      padding: '1rem .5rem',
      borderBottom: '2px solid white',
      width: '100%',
    },
    bmItemHover: {
      background: '#a90000',
    },
    bmOverlay: {
      background: 'rgba(0, 0, 0, 0.3)',
    },
  };

  return (
    <>
      <Head>
        <title>Rami Ismail</title>
        <link rel='icon' href='/favicon.ico' />
        <meta name='viewport' content='initial-scale=1.0, width=device-width' />
      </Head>
      <header className={styles.mobile}>
        <Menu
          customCrossIcon={<img src='/rami_symbol_white.svg' />}
          right
          styles={burgerStyles}
          width={'100%'}
        >
          <a className='menu-item' href='/'>
            Home
          </a>
          <a className='menu-item' href='/about'>
            About
          </a>

          <a className='menu-item' href='/blog'>
            Blog
          </a>
          <a className='menu-item' href='/works'>
            Works
          </a>
          <a className='menu-item' href='/bookings'>
            Bookings
          </a>
          <a className='menu-item' href='/contact'>
            Contact
          </a>
          <ul className={styles.social}>
            <li className={styles.socialIcon}>
              <a href='mailto:info@ramiismail.com'>
                <img src='/social/envelope_solid.svg' alt='email' />
              </a>
            </li>
            <li className={styles.socialIcon}>
              <a
                href='http://twitch.tv/tha_rami'
                target='_blank'
                rel='noopener noreferrer'
              >
                <img src='/social/twitch.svg' alt='twitch' />
              </a>
            </li>
            <li className={styles.socialIcon}>
              <a
                href='http://twitter.com/tha_rami'
                target='_blank'
                rel='noopener noreferrer'
              >
                <img src='/social/twitter.svg' alt='twitter' />
              </a>
            </li>
            <li className={styles.socialIcon}>
              <a
                href=' http://instagram.com/tha.rami'
                target='_blank'
                rel='noopener noreferrer'
              >
                <img src='/social/instagram.svg' alt='instagram' />
              </a>
            </li>
          </ul>
        </Menu>
      </header>
      <div className={about ? styles.nopadding : styles.container}>
        <header className={about ? styles.about : styles.header}>
          <nav className={styles.menu}>
            <ul className={styles.nav}>
              <li>
                <img src='/flag.png' alt='rami logo' className={styles.logo} />
              </li>
              <li>
                <Link href='/'>
                  <a
                    className={`${styles.link}
                      ${router.pathname == '/' ? styles.activeLink : ''}`}
                  >
                    Home
                  </a>
                </Link>
              </li>
              <li>
                {' '}
                <Link href='/about'>
                  <a
                    className={`${styles.link}
                      ${router.pathname == '/about' ? styles.activeLink : ''}`}
                  >
                    About
                  </a>
                </Link>
              </li>
              <li>
                <Link href='/blog'>
                  <a
                    className={`${styles.link}
                      ${
                        router.pathname == '/blog' ||
                        router.pathname == '/blog/[slug]'
                          ? styles.activeLink
                          : ''
                      }`}
                  >
                    Blog
                  </a>
                </Link>
              </li>
              <li>
                <Link href='/works'>
                  <a
                    className={`${styles.link}
                      ${
                        router.pathname == '/works' ||
                        router.pathname == '/works/[slug]'
                          ? styles.activeLink
                          : ''
                      }`}
                  >
                    Works
                  </a>
                </Link>
              </li>
              <li>
                <Link href='/bookings'>
                  <a
                    className={`${styles.link}
                      ${
                        router.pathname == '/bookings' ? styles.activeLink : ''
                      }`}
                  >
                    Bookings
                  </a>
                </Link>
              </li>
              <li>
                {' '}
                <Link href='/contact'>
                  <a
                    className={`${styles.link}
                      ${
                        router.pathname == '/contact' ? styles.activeLink : ''
                      }`}
                  >
                    Contact
                  </a>
                </Link>
              </li>
            </ul>
          </nav>
          <ul className={styles.social}>
            <li>
              <a href='mailto:info@ramiismail.com'>
                <svg
                  className={styles.icon}
                  viewBox='0 0 512 384'
                  xmlns='http://www.w3.org/2000/svg'
                  fillRule='evenodd'
                  clipRule='evenodd'
                  strokeLinejoin='round'
                  strokeMiterlimit='2'
                >
                  <path
                    d='M502.3 126.8c3.9-3.1 9.7-.2 9.7 4.7V336c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V131.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 256c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9V48c0-26.5-21.5-48-48-48H48C21.5 0 0 21.5 0 48v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z'
                    fill='#fff'
                    fillRule='nonzero'
                  />
                </svg>
              </a>
            </li>
            <li>
              <a
                href='http://twitch.tv/tha_rami'
                target='_blank'
                rel='noopener noreferrer'
              >
                <svg
                  className={styles.icon}
                  viewBox='0 0 464 512'
                  xmlns='http://www.w3.org/2000/svg'
                  fillRule='evenodd'
                  clipRule='evenodd'
                  strokeLinejoin='round'
                  strokeMiterlimit='2'
                >
                  <path
                    d='M366.86 103.47h-38.63v109.7h38.63v-109.7zM260.69 103h-38.63v109.75h38.63V103zM96.52 0L0 91.42v329.16h115.83V512l96.53-91.42h77.25L463.38 256V0H96.52zm328.24 237.75l-77.22 73.12H270.3l-67.6 64v-64h-86.87V36.58h308.93v201.17z'
                    fill='#fff'
                    fillRule='nonzero'
                  />
                </svg>
              </a>
            </li>
            <li>
              <a
                href='http://twitter.com/tha_rami'
                target='_blank'
                rel='noopener noreferrer'
              >
                <svg
                  className={styles.icon}
                  viewBox='0 0 512 416'
                  xmlns='http://www.w3.org/2000/svg'
                  fillRule='evenodd'
                  clipRule='evenodd'
                  strokeLinejoin='round'
                  strokeMiterlimit='2'
                >
                  <path
                    d='M459.37 103.634c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954C87.39 82.842 165.035 124.425 252.1 128.974c-1.624-7.797-2.599-15.918-2.599-24.04C249.501 47.106 296.283 0 354.435 0c30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z'
                    fill='#fff'
                    fillRule='nonzero'
                  />
                </svg>
              </a>
            </li>
            <li>
              <a
                href=' http://instagram.com/tha.rami'
                target='_blank'
                rel='noopener noreferrer'
              >
                <svg
                  className={styles.icon}
                  viewBox='0 0 449 449'
                  xmlns='http://www.w3.org/2000/svg'
                  fillRule='evenodd'
                  clipRule='evenodd'
                  strokeLinejoin='round'
                  strokeMiterlimit='2'
                >
                  <path
                    d='M224.175 109.175c-63.6 0-114.9 51.3-114.9 114.9 0 63.6 51.3 114.9 114.9 114.9 63.6 0 114.9-51.3 114.9-114.9 0-63.6-51.3-114.9-114.9-114.9zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1-26.3 26.2-34.4 58-36.2 93.9-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9 26.3 26.2 58 34.4 93.9 36.2 37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zm-47.8 224.5c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9-32.6 0-102.7 2.6-132.1-9-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1 0-32.6-2.6-102.7 9-132.1 7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9 32.6 0 102.7-2.6 132.1 9 19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1 0 32.6 2.7 102.7-9 132.1z'
                    fill='#fff'
                    fillRule='nonzero'
                  />
                </svg>
              </a>
            </li>
          </ul>
        </header>
        <main>{children}</main>
      </div>
    </>
  );
}
