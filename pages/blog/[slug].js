import styles from '../../styles/slug.module.css';
import Layout from '../components/layout';
import { format } from 'date-fns';
import { documentToReactComponents } from '@contentful/rich-text-react-renderer';

export default function Slug({ post }) {
  const { title, author, date, coverImage, content, slug } = post.fields;

  return (
    <Layout>
      <div className={styles.singlePost}>
        {coverImage ? (
          <img
            className={styles.headerImage}
            src={coverImage.fields.file.url}
            alt=''
          />
        ) : null}
        <h1>{title}</h1>
        {date ? (
          <time className={styles.date} dateTime={date}>
            {format(new Date(date), 'LLLL	d, yyyy')}
          </time>
        ) : null}
        {author && (
          <span>
            <p className={styles.author}>{author.fields.name}</p>
          </span>
        )}
        <div>{documentToReactComponents(content)}</div>
      </div>
    </Layout>
  );
}

export async function getStaticProps(context) {
  // Create an instance of the Contentful JavaScript SDK
  const client = require('contentful').createClient({
    space: process.env.CONTENTFUL_SPACE_ID,
    accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
  });

  // Fetch all results where `fields.slug` is equal to the `slug` param
  const result = await client
    .getEntries({
      content_type: 'post',
      'fields.slug': context.params.slug,
    })
    .then((response) => response.items);

  // Since `slug` was set to be a unique field, we can be confident that
  // the only result in the query is the correct post.
  const post = result.pop();

  // If nothing was found, return an empty object for props, or else there would
  // be an error when Next tries to serialize an `undefined` value to JSON.
  if (!post) {
    return { props: {} };
  }

  // Return the post as props
  return {
    props: {
      post,
    },
  };
}

export async function getStaticPaths() {
  // Create an instance of the Contentful JavaScript SDK
  const client = require('contentful').createClient({
    space: process.env.CONTENTFUL_SPACE_ID,
    accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
  });

  // Query Contentful for all blog posts in the space
  const posts = await client
    .getEntries({ content_type: 'post' })
    .then((response) => response.items);

  // Map the result of that query to a list of slugs.
  // This will give Next the list of all blog post pages that need to be
  // rendered at build time.
  const paths = posts.map(({ fields: { slug } }) => ({ params: { slug } }));

  return {
    paths,
    fallback: false,
  };
}
