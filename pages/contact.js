import styles from '../styles/contact.module.css';
import Layout from './components/layout';
import Link from 'next/link';

export default function Contact() {
  return (
    <Layout>
      <div className={styles.contact}>
        <div className={styles.container}>
          <h1>Get in touch!</h1>
          <p>
            You can use the{' '}
            <Link href='/bookings'>
              <a>Bookings</a>
            </Link>{' '}
            page to book a timeslot with me to discuss X
          </p>
          <h3>or use the information below for your specific use cases</h3>
          <ul className={styles.list}>
            <li>
              <a className={styles.email} href='mailto:info@ramiismail.com'>
                info@ramiismail.com
              </a>{' '}
              for general questions or topics unlisted.
            </li>
            <li>
              {' '}
              <a className={styles.email} href='mailto: press@ramiismail.com'>
                press@ramiismail.com
              </a>{' '}
              for all media & interview contacts.
            </li>
            <li>
              <a className={styles.email} href='mailto: funding@ramiismail.com'>
                funding@ramiismail.com
              </a>{' '}
              for all inquiries concerning IndieFund or other funds.
            </li>
            <li>
              {' '}
              <a className={styles.email} href='mailto: events@ramiismail.com'>
                events@ramiismail.com
              </a>{' '}
              for all inquiries concerning events and talks.
            </li>
          </ul>
        </div>
      </div>
      <footer className={styles.footer}>
        <img src='/contact-brand.png' alt='footer-logo' />
      </footer>
    </Layout>
  );
}
