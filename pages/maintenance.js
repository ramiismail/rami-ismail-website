import styles from '../styles/maintenance.module.css';

export default function About() {
  return (
    <div className={styles.maintenance}>
      <div className={styles.content}>
        <img className={styles.symbol} src='/rami_symbol_white.svg' alt='' />
        <h1>We will be back soon!</h1>
        <p>
          Sorry for the inconvenience but we are performing some maintenance at
          the moment. We will be back online shortly!
        </p>
        <img className={styles.symbol} src='/public-sig.png' alt='' />
      </div>
    </div>
  );
}
