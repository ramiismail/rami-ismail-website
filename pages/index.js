import styles from '../styles/index.module.css';
import Layout from './components/layout';

export default function Index() {
  return (
    <Layout>
      <div className={styles.home}>
        <div className={styles.container}>
          <div className={styles.text}>
            <h1 className={styles.title}>Hey, I'm Rami</h1>
            <p>I am a game developer & a public speaker</p>
            <p>
              I am also the executive director of{' '}
              <a
                className={styles.link}
                href='https://gamedev.world/'
                target='_blank'
                rel='noopener noreferrer'
              >
                gamedev.world
              </a>
              , the creator of{' '}
              <a
                className={styles.link}
                href='https://dopresskit.com/'
                target='_blank'
                rel='noopener noreferrer'
              >
                presskit()
              </a>
              , and an industry ambassador focused on creating fair & global
              opportunities for game development.
            </p>
            <p>
              You might know me from my work at{' '}
              <a
                className={styles.link}
                href='https://vlambeer.com/'
                target='_blank'
                rel='noopener noreferrer'
              >
                Vlambeer
              </a>
              ,{' '}
              <a
                className={styles.link}
                href='https://twitter.com/tha_rami'
                target='_blank'
                rel='noopener noreferrer'
              >
                my Twitter
              </a>{' '}
              , or from my appearances on games & gaming related podcasts &
              livestreams.
            </p>
          </div>
          <img
            className={styles.photo}
            src='/landing-photo.jpg'
            alt='rami ismail photo'
          />
        </div>
      </div>
    </Layout>
  );
}
