import styles from '../styles/about.module.css';
import Layout from './components/layout';

export default function About() {
  return (
    <Layout about>
      <div className={styles.about}>
        <div className={styles.content}>
          <img className={styles.symbol} src='/rami_symbol_white.svg' alt='' />
          <h1>So who am I?</h1>
          <p>
            Over the past 30-ish years, I've worked on a plethora of
            commercially and critically successful games for almost every major
            platform. I have also worked on several award-winning more
            experimental titles. While I am trained primarily as a programmer,
            being an independent developer has taught me to effectively fulfill
            the roles of producer, marketeer, community manager, and designer.
          </p>
          <img className={styles.symbol} src='/public-sig.png' alt='' />
        </div>
      </div>
    </Layout>
  );
}
