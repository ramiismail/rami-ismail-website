import '../styles/global.css';
import Router from 'next/router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import Maintenance from './maintenance';

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

function App({ Component, pageProps }) {
  if (process.env.NEXT_PUBLIC_MAINTENANCE === 'true') {
    return <Maintenance />;
  }
  return <Component {...pageProps} />;
}
export default App;

// export default function App({ Component, pageProps }) {
//   return <Component {...pageProps} />;
// }
