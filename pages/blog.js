import styles from '../styles/blog.module.css';
import Layout from './components/layout';
import Link from 'next/link';
import { format } from 'date-fns';
import { fetchBlogEntries } from './api';

export default function Blog({ posts }) {
  const heroPost = posts[0];
  const morePosts = posts.slice(1);
  return (
    <>
      <Layout blog>
        {heroPost.coverImage !== null && (
          <div className={styles.blog}>
            <div className={styles.latest}>
              {heroPost.coverImage ? (
                <img src={heroPost.coverImage.fields.file.url} alt='' />
              ) : null}
              <div className={styles.post}>
                <h2>
                  <Link as={`/blog/${heroPost.slug}`} href='/blog/[slug]'>
                    <a className={styles.title}>{heroPost.title}</a>
                  </Link>
                </h2>
                {heroPost.date ? (
                  <time className={styles.date} dateTime={heroPost.date}>
                    {format(new Date(heroPost.date), 'LLLL d, yyyy')}
                  </time>
                ) : null}
                <p className={styles.excerpt}>{heroPost.excerpt}</p>
                <Link as={`/blog/${heroPost.slug}`} href='/blog/[slug]'>
                  <button className={styles.readmore}>read more</button>
                </Link>
              </div>
            </div>
            <div className={styles.mobile}>
              <div className={styles.preview}>
                {heroPost.coverImage ? (
                  <img src={heroPost.coverImage.fields.file.url} alt='' />
                ) : null}
                <h2>
                  <Link as={`/blog/${heroPost.slug}`} href='/blog/[slug]'>
                    <a className={styles.title}>{heroPost.title}</a>
                  </Link>
                </h2>
                {heroPost.date ? (
                  <time className={styles.date} dateTime={heroPost.date}>
                    {format(new Date(heroPost.date), 'LLLL	d, yyyy')}
                  </time>
                ) : null}
                <p className={styles.excerpt}>{heroPost.excerpt}</p>
                <Link as={`/blog/${heroPost.slug}`} href='/blog/[slug]'>
                  <button className={styles.readmore}>read more</button>
                </Link>
              </div>
            </div>

            <div className={styles.posts}>
              {morePosts.map((p) => {
                return (
                  <div key={p.date} className={styles.preview}>
                    <img
                      src={p.coverImage ? p.coverImage.fields.file.url : ''}
                      alt=''
                    />
                    <h2>
                      <Link as={`/blog/${p.slug}`} href='/blog/[slug]'>
                        <a className={styles.title}>{p.title}</a>
                      </Link>
                    </h2>
                    <time className={styles.date} dateTime={p.date}>
                      {p.date ? format(new Date(p.date), 'LLLL	d, yyyy') : null}
                    </time>
                    <p className={styles.excerpt}>{p.excerpt}</p>
                    <Link as={`/blog/${p.slug}`} href='/blog/[slug]'>
                      <button className={styles.readmore}>read more</button>
                    </Link>
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </Layout>
    </>
  );
}

export async function getStaticProps(context) {
  const res = await fetchBlogEntries();
  const posts = await res.map((p) => {
    return p.fields;
  });

  return {
    props: {
      posts,
    },
  };
}
